@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="input-group">
                        <input type="text" id="queryText" placeholder="Enter the product you are looking for" class="form-control" />
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" id="searchButton">Search!</button>
                      </span>

                    </div>
                </div>

            </div>
            <searchresult v-show="items.length>0" :items="items" :search-results="searchResults" :search-query="searchQuery"></searchresult>
        </div>
    </div>
</div>
@endsection
@section('page-scripts')
    var app; 
    $(document).ready(function(){
        app = new Vue({
            el:'#app',
            data:{
                searchQuery:'Hello World',
                searchResults:100,
                items:[
                    /*{
                        imgUrl:"https://i5.walmartimages.com/asr/bdb29314-3a3e-49a1-b694-34dff0461693_1.869b329817f9d9f35aadb656177cd8b2.jpeg?odnHeight=100&odnWidth=100&odnBg=FFFFFF",
                        price:"195.00",
                        name:"Apple iPod touch 16GB, Assorted Colors",
                        description:"<b>Apple iPod touch 16GB, Assorted Colors:</b><div style=&quotmargin-left: 2em&quot><b>Key Features:</b></div><ul><li>4-inch Retina display</li><li>A8 with M8 motion coprocessor</li><li>8MP iSight &amp FaceTime cameras</li><li>1080p HD video recording</li><li>802.11ac Wi-Fi &amp Bluetooth 4.1</li><li>Up to 40 hours audio playback</li></ul><div style=&quotmargin-left: 2em&quot><b>Legal</b><br>iPod models are not available in all colors at all resellers.<br>Membership required. Requires initial sign-up. At the end of the trial period, the membership will automatically renew and payment method will be charged on a monthly basis until autorenewal is turned off in account settings.<br>FaceTime calling requires a FaceTime-enabled device with a Wi-Fi connection.<br> Display size is measured diagonally.<br>Rechargeable batteries have a limited number of charge cycles and may eventually need to be replaced. Battery life and number of charge cycles vary by use and settings. See www.apple.com/batteries for more information.<br>TM and (C) 2015 Apple Inc. All rights reserved.</div>"
                    }*/
                ]
                
            }
        });
        
        $("#searchButton").click(function(){
            search();
        })
    });
    
    function search(){
        var searchQuery = $("#queryText").val();
        jQuery.getJSON( "/search/query", { query:  searchQuery})
            .done(function( json ) {
                app.items = json;
                app.searchQuery = searchQuery;
            })
            .fail(function( jqxhr, textStatus, error ) {
                var err = textStatus + ", " + error;
                console.log( "Request Failed: " + err );
            });
    }
@endsection
