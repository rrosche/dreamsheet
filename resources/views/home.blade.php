@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Products <span class="align-right">Add Produc</span></div>

                <div class="panel-body">
                    @foreach($products as $index=>$product)
                         <div class="row">
                            @if($index > 0)
                            <hr/>
                            @endif
                            <div class="row">
                                <img class="col-sm-3" src="{{$product->image_url}}" />
                                <h3 class="col-md-6">{{$product->name}}</h3> 
                                <div class="col-md-3">
                                    <span><h4>${{$product->price}}</h4>
                                    <button class="btn btn-danger" onClick="remove({{$product->id}})">-</button>
                                    <div class="clearfix"></div>
                                    <br/>
                                    <button class="btn btn-primary" data-toggle="collapse" data-target="#description{{$index}}">Description</button>
                                    <div class="clearfix"></div><br/>
                                    
                                </div>
                            </div>
                            <br/>
                            <div class="row collapse" id="description{{$index}}">
                                <blockquote class="col-md-offset-1">
                                    {!!$product->description!!}
                                </blockquote>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-scripts')
function remove(id){
    jQuery.getJSON('product/remove/'+id);
}
@endsection
