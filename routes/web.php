<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    if(Auth::check()){
        return redirect('home');
    }
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/search','SearchController@index');
Route::get('/search/query','SearchController@search');
Route::post('/product/add', 'ProductController@add');
Route::get('/product/remove/{id}','ProductController@Remove');
