<?php

namespace App\Repository;


interface WalmartRepo {
    public function searchForProduct($searchQuery, &$searchDetails);
    
    public function getProductDetails($productId);
    
    public function findProductByUpc($upcCode,&$searchDetails);
}