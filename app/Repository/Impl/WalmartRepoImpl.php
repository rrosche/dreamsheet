<?php
//http://api.walmartlabs.com/v1/search?query=orange+juice&format=json&apiKey=22dffewkykncvxmn7w9hta5m

namespace App\Repository\Impl;

use App\Domain\Product\WalmartProduct;
use App\Domain\Search\WalmartSearchDetails;
use App\Repository\WalmartRepo;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;

class WalmartRepoImpl implements WalmartRepo {

    
    public function searchForProduct($searchQuery, &$searchDetails){
        Log::info("In Search");
        $client = new Client([
            'base_uri'=> env('WALMART_URL','')
        ]);
        $promise = $client->requestAsync('GET','search',['query'=>[
            'numItems' => 25,
            'query' => $searchQuery,
            'apiKey' => env('WALMART_KEY',''),
            'format' => 'json',
        ]]);
        
        $promise->then(
            function (ResponseInterface $res) use (&$searchDetails){
                $searchDetail =  $this->getSearchDetails(
                    json_decode($res->getBody())
                );
                $searchDetails[] = $searchDetail;
            },
            function (RequestException $e) {
                Log::error("Error in request", ['error' => $e]);
                echo $e->getMessage() . "\n";
                echo $e->getRequest()->getMethod();
            }
        );
        
        return $promise;
    }
    
    public function getProductDetails($productId){
        
    }
    
    public function findProductByUpc($upcCode,&$searchDetails){
        $client = new Client([
            'base_uri'=> env('WALMART_URL','')
        ]);
        $promise = $client->requestAsync('GET','items',['query'=>[
            'upc' => "$upcCode",
            'apiKey' => env('WALMART_KEY',''),
            'format' => 'json',
        ]]);
        
        $promise->then(
            function (ResponseInterface $res) use (&$searchDetails){
                $searchDetail =  $this->getSearchDetails(
                    json_decode($res->getBody())
                );
                $searchDetails[] = $searchDetail;
            },
            function (RequestException $e) {
                Log::error("Error in request", ['error' => $e]);
                echo $e->getMessage() . "\n";
                echo $e->getRequest()->getMethod();
            }
        );
        
        return $promise;
    }
    
    private function isValidProduct($product){
        $isValid = true;
        $isValid &= $product->itemId != null;
        $isValid &= $product->name != null;
        $isValid &= $product->salePrice != null;
        $isValid &= $product->upc != null;
        return $isValid;
    }
    
    private function getSearchDetails($responseArray){
        $searchDetails = new WalmartSearchDetails();
        if(!empty($responseArray->totalResults))
            $searchDetails->setTotalResults($responseArray->totalResults);
        
        if(!empty($responseArray->start))
            $searchDetails->setPageStart($responseArray->start);
        
        if(!empty($responseArray->numItems))
            $searchDetails->setPageEnd($responseArray->numItems);
       
        foreach($responseArray->items as $product){
            try{
                $walmartProduct = new WalmartProduct();
                if(!$this->isValidProduct($product)){
                    Log::info("Product information invalid", ["Product" => $product]);
                    continue;
                }
               
                
                if($product->itemId)
                    $walmartProduct->setId($product->itemId);
                
                if($product->name)
                    $walmartProduct->setName($product->name);
                
                if($product->salePrice)
                    $walmartProduct->setPrice($product->salePrice);
                
                if($product->upc)
                    $walmartProduct->setUpc($product->upc);
                
                if($product->longDescription)
                    $walmartProduct->setDescription(str_replace(['&lt;','&gt;'],['<','>'],$product->longDescription));
                else
                    $walmartProduct->setDescription("No Description Provided.");
                
                if($product->thumbnailImage)
                    $walmartProduct->setImageUrl($product->thumbnailImage);
                
                if($product->productUrl)
                    $walmartProduct->setUrl($product->productUrl);
                
                if($product->availableOnline)
                    $walmartProduct->setIsAvailableOnline($product->availableOnline);
                else
                    $walmartProduct->setIsAvailableOnline(false);
                
                $searchDetails->addProduct($walmartProduct);
            }catch(\Exception $e){
                Log::info("Error adding product to result list", ["Error"=>$e,"Product Json" => $product]);
            }
        }
        
        return $searchDetails;
    }
}