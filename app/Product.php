<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $fillable = [
        'lookup_code',
        'name',
        'description',
        'price',
        'upc_code',
        'image_url',
        'product_url',
    ];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
}
