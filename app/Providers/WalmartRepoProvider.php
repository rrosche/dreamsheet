<?php

namespace App\Providers;

use App\Repository\WalmartRepo;
use App\Repository\Impl\WalmartRepoImpl;
use Illuminate\Support\ServiceProvider;

class WalmartRepoProvider extends ServiceProvider {
    
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(WalmartRepo::class, function($app){
           return new WalmartRepoImpl();
        });
    }
    
}