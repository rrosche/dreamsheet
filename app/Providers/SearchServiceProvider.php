<?php

namespace App\Providers;

use App\Repository\WalmartRepo;
use App\Service\SearchService;
use App\Service\Impl\SearchServiceImpl;
use Illuminate\Support\ServiceProvider;

class SearchServiceProvider extends ServiceProvider {
    
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(SearchService::class, function($app){
           return new SearchServiceImpl(
               $app->tagged('searchServices')
           );
        });
    }
    
}