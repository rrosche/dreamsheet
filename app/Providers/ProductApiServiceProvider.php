<?php

namespace App\Providers;

use App\Repository\WalmartRepo;
use App\Service\WalmartService;
use App\Service\Impl\WalmartServiceImpl;
use Illuminate\Support\ServiceProvider;

class ProductApiServiceProvider extends ServiceProvider {
    
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(WalmartService::class, function($app){
           return new WalmartServiceImpl(
               $app->make(WalmartRepo::class)
           );
        });
        
        $this->app->tag([WalmartService::class], 'searchServices');    
        
    }
    
}