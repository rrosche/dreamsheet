<?php

namespace App\Service;

interface ApiProductSearch{
    
    public function searchForProduct($searchQuery, &$searchDetails);
    public function searchForUpc($upc,&$searchDetails);
}