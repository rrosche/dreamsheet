<?php

namespace App\Service;

interface WalmartService{
    
    public function getProductDetails($productId);
    
    public function findProductByUpc($upcCode, &$searchDetails);
}