<?php

namespace App\Service\Impl;

use App\Jobs\SearchProductApiJob;
use App\Service\SearchService;
use App\Service\ApiProductSearch;
use Illuminate\Foundation\Bus\DispatchesJobs;

class SearchServiceImpl implements SearchService {
    use DispatchesJobs;
    
    private $searchServices;
    
    public function __construct($searchServices){
        $this->searchServices = $searchServices;    
    }
    
    public function searchForProduct($searchQuery){
        $promises = array();
        $searchDetails = array();
        $done = false;
        foreach($this->searchServices as $key => $service){
            $promises[] = $service->searchForProduct($searchQuery,$searchDetails);
        }
        foreach($promises as $promise){
            $promise->wait();
        }
        return $searchDetails;
    }
    
    public function searchForUpc($upc){
        $promises = array();
        $searchDetails = array();
        $done = false;
        foreach($this->searchServices as $key => $service){
            $promises[] = $service->searchForUpc($upc,$searchDetails);
        }
        foreach($promises as $promise){
            $promise->wait();
        }
        return $searchDetails;
    }
}