<?php

namespace App\Service\Impl;

use App\Repository\WalmartRepo;
use App\Service\ApiProductSearch;
use App\Service\WalmartService;

class WalmartServiceImpl implements WalmartService, ApiProductSearch {
    
    private $walmartRepo;
    
    public function __construct(WalmartRepo $walmartRepo){
        $this->walmartRepo = $walmartRepo;    
    }
    
    public function searchForProduct($searchQuery, &$searchDetails){
        return $this->walmartRepo->searchForProduct($searchQuery, $searchDetails);
    }
    
    public function searchForUpc($upc,&$searchDetails){
        return $this->walmartRepo->findProductByUpc($upc,$searchDetails);
    }
    
    public function getProductDetails($productId){
        
    }
    
    public function findProductByUpc($upcCode,&$searchDetails){
        
    }
}