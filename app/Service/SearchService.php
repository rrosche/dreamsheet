<?php

namespace App\Service;

interface SearchService{
    
    public function searchForProduct($searchQuery);
    public function searchForUpc($upc);
}