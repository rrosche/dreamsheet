<?php

namespace App\Domain\Search;

abstract class SearchDetails implements \JsonSerializable{
    private $totalResults;
    private $pageStart;
    private $pageEnd;
    private $products;
    
    public function __construct($pageStart = 1, $pageEnd = null){
        $this->setPageStart($pageStart);
        $this->setPageEnd($pageEnd);
    }
    
    public function getTotalResults(){
        return $this->totalResults;
    }
    
    public function setTotalResults($totalResults){
        $this->totalResults = $totalResults;
    }
    
    public function getPageStart(){
        return $this->pageStart;
    }
    
    public function setPageStart($pageStart){
        $this->pageStart = $pageStart;
    }
    
    public function getPageEnd(){
        return $this->pageEnd;
    }
    
    public function setPageEnd($pageEnd){
        $this->pageEnd = $pageEnd;
    }
    
        
    
    public function setProducts($products){
        $this->products = $products;
    }
    
    public function getProducts(){
        return array_values($this->products);
    }
    
    public function addProduct($product){
        $this->products[$product->getUpc()] = $product;
    }
    
    public function getProductsArray(){
        return $this->products;
    }
    
    public function jsonSerialize(){
        return array(
            'totalResults' => $this->getTotalResults(),
            'pageStart' => $this->getPageStart(),
            'pageEnd' => $this->getPageEnd(),
            'products' => $this->getProducts()
        );
    }
}