<?php

namespace App\Domain\Product;

use App\Domain\Product\Product;

class WalmartProduct extends Product{
    
    private $availableOnline;
    
    public function __construct(){
        $this->setOrigin("Walmart.com");
    }

    
    public function isAvailableOnline(){
        return $this->availableOnline;
    }
    
    public function setIsAvailableOnline($availableOnline){
        $this->availableOnline = $availableOnline;
    }
    
    public function jsonSerialize(){
        $parentJson = parent::JsonSerialize();
        return array_merge(
            $parentJson, 
            array(
                "availableOnline" => $this->isAvailableOnline()
            )
        );
    }
}