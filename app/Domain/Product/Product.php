<?php

namespace App\Domain\Product;

abstract class Product implements \JsonSerializable {
    
    private $itemId;
    private $name;
    private $price;
    private $upc;
    private $description;
    private $imageUrl;
    private $url;
    private $available;
    private $origin;
    
    public function getId(){
        return $this->itemId;
    }
    
    public function setId($itemId){
        $this->itemId = $itemId;
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function setName($name){
        $this->name = $name;
    }
    
    public function getPrice(){
        return $this->price;
    }
    
    public function setPrice($price){
        $this->price = $price;
    }
    
    public function getUpc(){
        return $this->upc;
    }
    
    public function setUpc($upc){
        $this->upc = $upc;
    }
    
    public function getDescription(){
        return $this->description;
    }
    
    public function setDescription($description){
        $this->description = $description;
    }
    
    public function getImageUrl(){
        return $this->imageUrl;
    }
    
    public function setImageUrl($imageUrl){
        $this->imageUrl = $imageUrl;
    }
    
    public function getUrl(){
        return $this->url;
    }
    
    public function setUrl($url){
        $this->url = $url;
    }
    
    public function setOrigin($origin){
        $this->origin = $origin;
    }
    
    public function getOrigin(){
        return $this->origin;
    }
    
    public function jsonSerialize(){
        return array(
            "id" => $this->getId(),
            "upc" => $this->getUpc(),
            "name" => $this->getName(),
            "price" => $this->getPrice(),
            "description" => $this->getDescription(),
            "url" => $this->getUrl(),
            "imageUrl" => $this->getImageUrl(),
            "origin" => $this->getOrigin()
        );
    }
    
}