<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function add(Request $request){
        $user = $request->user();
        $upc = $request->input('upc');
        $product = $request->session()->get('products')[$upc];
        if($product){
            $user->products()->create([
                    'lookup_code' => $product->getId(),
                    'name' => $product->getName(),
                    'description' => $product->getDescription(),
                    'price' => $product->getPrice(),
                    'upc_code' => $product->getUpc(),
                    'image_url' => $product->getImageUrl(),
                    'product_url' => $product->getUrl(),
                ]);
            return response()->json(['success' => true]);
        }else{
            return response()->json(['success'=>false]);
        }
    }
    
    public function remove(Request $request,$id){
        $user = $request->user();
        $user->products()->where('id',$id)->delete();
    }

}
