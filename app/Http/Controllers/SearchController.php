<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Service\SearchService;

class SearchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('search');
    }
    
    public function search(Request $request, SearchService $searchService){
        if($request->input('query')){
            $response = $searchService->searchForProduct($request->input('query'));
        }else if($request->input('upc')){
            $response = $searchService->searchForUpc($request->input('upc'));
        }
        
        $productsArray = array();
        foreach($response as $searchResult){
            $productsArray = array_merge($productsArray, $searchResult->getProductsArray());
        }
        $request->session()->put('products',$productsArray);
        return response()->json(array_values($productsArray));
    }
}
