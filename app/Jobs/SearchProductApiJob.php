<?php

namespace App\Jobs;

use App\Service\ApiProductSearch;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SearchProductApiJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    
    private $response;
    private $searchQuery;
    private $searchService;
    private $callBack;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ApiProductSearch $searchService, $searchQuery, $callBack)
    {
        $this->searchQuery = $searchQuery;
        $this->searchService = $searchService;
        $this->callBack = $callBack;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->response = $this->searchService->searchForProduct($this->searchQuery);
        $this->callBack__invoke();
    }
    
    public function getResponse(){
        return $this->response;
    }
}
